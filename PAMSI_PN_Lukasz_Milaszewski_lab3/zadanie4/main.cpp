#include <iostream>
#include <vector>
#include <math.h>

using namespace std;

vector <int> tower1;
vector <int> tower2;
vector <int> tower3;

void push1(int data) {
  tower1.push_back(data);
}

int pop1() {
  if (tower1.empty() == 1)
    return 777;
  int temp = tower1.back();
  tower1.pop_back();
  return temp;
}

void push2(int data) {
  tower2.push_back(data);
}
int pop2() {
  if (tower2.empty() == 1)
    return 777;
  int temp = tower2.back();
  tower2.pop_back();
  return temp;
}

void push3(int data) {
  tower3.push_back(data);
}

int pop3() {
  if (tower3.empty() == 1)
    return 777;
  int temp = tower3.back();
  tower3.pop_back();
  return temp;
}

display1() {
  cout << "wieza1: ";
  for (int i = 0; i< tower1.size(); i++)
    cout << tower1[i] << " ";
  cout << endl;
}

display2() {
  cout << "wieza2: ";
  for (int i = 0; i< tower2.size(); i++)
    cout << tower2[i] << " ";
  cout << endl;
}

display3() {
  cout << "wieza3: ";
  for (int i = 0; i< tower3.size(); i++)
    cout << tower3[i] << " ";
  cout << endl;
}
int topStack() {
  if (tower1.empty() != 1 && tower1.back() == 1 )
  {
      return 1;
  }

   if (tower2.empty() != 1 && tower2.back() == 1)
  {
      return 2;
  }
   if (tower3.empty() != 1 && tower3.back() == 1 )
   {
       return 3;
   }

}

void toh(int n)
{
  int i, x, a, b;

  for (i = 0; i <(pow(2,n)); i++)
  {
    display1();
    display2();
    display3();
    cout << endl;
    x = topStack();
    if (i % 2 == 0)
    {
      if (x == 1)
        push3(pop1());
      if (x == 2)
        push1(pop2());
      if (x == 3)
        push2(pop3());
    }
    else
    {
      if (x == 1)
      {
        a = pop2();
        b = pop3();
        if (a < b && b != 777)
        {
          push3(b);
          push3(a);
        }
        if (a > b && a != 777)
        {
          push2(a);
          push2(b);
        }
        if (b == 777)
          push3(a);
        if (a == 777)
          push2(b);
      }
      else if (x == 2)
      {
        a = pop1();
        b = pop3();
        if (a < b && b != 777)
        {
          push3(b);
          push3(a);
        }
        if (a > b && a != 777)
        {
            push1(a);
            push1(b);
        }
        if (b == 777)
          push3(a);
        if (a == 777)
          push1(b);
      }
      else if (x == 3)
      {
        a = pop1();
        b = pop2();
        if (a < b && b != 777)
        {
          push2(b);
          push2(a);
        }
        if (a > b && a != 777)
        {
          push1(a);
          push1(b);
        }
        if (b == 777)
          push2(a);
        if (a == 777)
          push1(b);
      }
    }
  }
}

int main()
{
    int n, i;
    cout<<"Podaj wysokosc wiezy: ";
    cin>>n;
    for (i = n; i >= 1; i--)
    {
        push1(i);
    }
    toh(n);
    return 0;
}
