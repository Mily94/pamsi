#include "menu.h"
#include "queue.h"

using namespace std;

void Menu::displayMenu()
{
  Queue s;
  int option;
  do
  {
    cout << "1.Dodaj element do kolejki" << endl;
    cout << "2.Usun element z kolejki" << endl;
    cout << "3.Wyswietl kolejke" << endl;
    cout << "4.Usun kolejke" << endl;
    cout << "0.Koniec " << endl;
    cout << "Wybierz opcje: ";
    cin >> option;
    switch(option)
    {
      case 1:
        s.push();
        break;
      case 2:
        s.pop();
        break;
      case 3:
        s.display();
        break;
      case 4:
        s.deleteQueue();
        break;
    }
  } while (option != 0);
}
