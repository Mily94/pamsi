#ifndef Queue_H
#define Queue_H
#include <iostream>
#include <queue>
using namespace std;
class Queue {
private:
  queue <int> kolejka;
  queue <int> temp;
public:
  void pop();
  void push();
  void display();
  void deleteQueue();
};

#endif // Queue_H
