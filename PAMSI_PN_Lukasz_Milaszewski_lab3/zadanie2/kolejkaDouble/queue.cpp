#include "queue.h"


Queue::Queue()
{
  counter = 1;
  size = 2;
  array = new int [1];
}

void Queue::push()
{
  if (counter == 1)
  {
    cout << "----------------------------" << endl;
    cout << "Podaj wartosc: ";
    cin >> array[0];
    cout << "Element zostal dodany." << endl;
    cout << "----------------------------" << endl;
    counter++;
  }
  else
  {
    if (counter == size)
    {
      int* newArray = new int[size*2];
      for(int i=0;i<size*2;i++)
      {
        if(i < counter)
          newArray[i] = array[i];
      }
      delete [] array;
      array = newArray;
      size *= 2;
    }
      cout << "----------------------------" << endl;
      cout << "Podaj wartosc: ";
      cin >> array[counter-1];
      cout << "Element zostal dodany." << endl;
      cout << "----------------------------" << endl;
      counter++;
  }
}

void Queue::pop()
{
  cout << "----------------------------" << endl;
  if (counter == 1)
    cout << "Nie mozna usunac, poniewaz kolejka jest pusta." << endl;
  else
  {
  cout << "Element zostal usuniety: " << array[0];
  int* newArray = new int[size*2];
  for(int i=0;i<counter-2;i++)
    newArray[i] = array[i+1];
  counter--;
  size = counter;
  delete [] array;
  array = newArray;
  }
  cout << endl;
  cout << "----------------------------" << endl;
}

void Queue::display()
{
  cout << "----------------------------" << endl;
  if ( counter == 1)
    cout << "Kolejka jest pusta." << endl;
  else {
    for (int i = 0; i < counter-1; i++) {
      cout << array[i] << " ";
    }
  }
  cout << endl;
  cout << "----------------------------" << endl;
}

void Queue::deleteQueue()
{
  delete [] array;
  cout << "----------------------------" << endl;
  cout << "Usunieto kolejke." << endl;
  cout << "----------------------------" << endl;
  counter = 1;
  size = 2;
  array = new int [1];

}

Queue::~Queue()
{
  delete [] array;
}



