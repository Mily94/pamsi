#ifndef queue_H
#define queue_H
#include <iostream>
using namespace std;
class Queue
{
private:
  int counter;
  int size;
  int *array;

public:
  void pop();
  void push();
  void display();
  void deleteQueue();
  Queue();
  ~Queue();
};

#endif // queue_H
