#include "queue.h"


Queue::Queue()
{
  counter = 1;
  array = new int[1];
}

void Queue::push()
{
  if (counter == 1)
  {
    counter++;
    cout << "----------------------------" << endl;
    cout << "Podaj wartosc: ";
    cin >> array[0];
    cout << "Element zostal dodany." << endl;
    cout << "----------------------------" << endl;
  }
  else
  {
  int* newArray = new int[counter];
  for (int i = 0; i < counter-1; i++)
    newArray[i] = array[i];
  delete [] array;
  array = newArray;
  cout << "----------------------------" << endl;
  cout << "Podaj wartosc: ";
  cin >> array[counter-1];
  cout << "Element zostal dodany." << endl;
  cout << "----------------------------" << endl;
  counter++;
  }
}

void Queue::pop()
{
  cout << "----------------------------" << endl;
  if (counter == 1)
    cout << "Nie mozna usunac, poniewaz kolejka jest pusty." << endl;
  else {
  cout << "Element zostal usuniety: " << array[0];
  }
  cout << endl;
  cout << "----------------------------" << endl;

  int* newArray = new int[counter];
  for(int i=0;i<counter;i++)
    newArray[i] = array[i+1];
  counter--;
  delete [] array;
  array = newArray;
}

void Queue::display()
{
  cout << "----------------------------" << endl;
  if ( counter == -1)
    cout << "Kolejka jest pusta" << endl;
  else
  {
    for (int i = 0; i < counter-1; i++)
      cout << array[i] << " ";  
  }
  cout << endl;
  cout << "----------------------------" << endl;
}

void Queue::deleteQueue()
{
  cout << "----------------------------" << endl;
  cout << "Usunieto kolejke." << endl;
  cout << "----------------------------" << endl;
  counter = 1;
  delete [] array;
  array = new int[1];
}

Queue::~Queue()
{
  delete [] array;
}



