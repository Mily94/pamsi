#ifndef STACK_H
#define STACK_H
#include <iostream>
#include <stack>
using namespace std;
class Stack {
private:
  stack <int> stos;
  stack <int> temp;
public:
  void pop();
  void push();
  void display();
  void deleteStack();
};

#endif // STACK_H
