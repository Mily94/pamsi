#include "stack.h"


Stack::Stack()
{
  counter = 1;
  size = 2;
  array = new int [1];
}

void Stack::push()
{
  if (counter == 1)
  {
    cout << "----------------------------" << endl;
    cout << "Podaj wartosc: ";
    cin >> array[0];
    cout << "Element zostal dodany." << endl;
    cout << "----------------------------" << endl;
    counter++;
  }
  else
  {
    if (counter == size)
    {
      int* newArray = new int[size*2];
      for(int i=0;i<size*2;i++)
      {
        if(i < counter)
          newArray[i] = array[i];
      }
      delete [] array;
      array = newArray;
      size *= 2;
    }
      cout << "----------------------------" << endl;
      cout << "Podaj wartosc: ";
      cin >> array[counter-1];
      cout << "Element zostal dodany." << endl;
      cout << "----------------------------" << endl;
      counter++;
  }
}

void Stack::pop()
{
  cout << "----------------------------" << endl;
  if (counter == 1)
    cout << "Nie mozna usunac, poniewaz stos jest pusty." << endl;
  else
  {
  cout << "Element zostal usuniety: " << array[counter-2];
  int* newArray = new int[2*size];
  for(int i=0;i<counter-2;i++)
    newArray[i] = array[i];
  counter--;
  size = counter;
  delete [] array;
  array = newArray;
  }
  cout << endl;
  cout << "----------------------------" << endl;
}

void Stack::display()
{
  cout << "----------------------------" << endl;
  if ( counter == 1)
    cout << "Stos jest pusty" << endl;
  else {
    for (int i = 0; i < counter-1; i++) {
      cout << array[i] << " ";
    }
  }
  cout << endl;
  cout << "----------------------------" << endl;
}

void Stack::deleteStack()
{
  delete [] array;
  cout << "----------------------------" << endl;
  cout << "Usunieto stos." << endl;
  cout << "----------------------------" << endl;
  counter = 1;
  size = 2;
  array = new int [1];

}

Stack::~Stack()
{
  delete [] array;
}



