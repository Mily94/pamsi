#ifndef STACK_H
#define STACK_H
#include <iostream>
using namespace std;
class Stack
{
private:
  int counter;
  int size;
  int *array;

public:
  void pop();
  void push();
  void display();
  void deleteStack();
  Stack();
  ~Stack();
};

#endif // STACK_H
