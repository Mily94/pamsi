#include "menu.h"
#include "stack.h"

using namespace std;

void Menu::displayMenu()
{
  Stack s;
  int option;
  do
  {
    cout << "1.Dodaj element do stosu" << endl;
    cout << "2.Usun element ze stosu" << endl;
    cout << "3.Wyswietl stos" << endl;
    cout << "4.Usun stos" << endl;
    cout << "0.Koniec " << endl;
    cout << "Wybierz opcje: ";
    cin >> option;
    switch(option)
    {
      case 1:
        s.push();
        break;
      case 2:
        s.pop();
        break;
      case 3:
        s.display();
        break;
      case 4:
        s.deleteStack();
        break;
    }
  } while (option != 0);
}
