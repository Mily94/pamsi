#include "kruskal.h"
#include "wrapper.h"

#include <algorithm>
#include <iostream>

using namespace std;

Wrapper::Wrapper()
{

}

Wrapper::~Wrapper()
{

}
void Wrapper::init(int vertices)
{
  _graph = new Graph();
  _graph -> _matrix = matrix(vertices);

  for ( vector <int>& s : _graph -> _matrix)
    s = vector<int>(vertices);

  _graph -> _list = list(vertices);
}

void Wrapper::build_from_random(int v, int edges)
{
  init(v);

  vector <vector<int> > l(v);

  for (int i = 0; i < (int) l.size(); i++)
    l[i].push_back(i);

  int e = 0;
  while (e < edges)
  {
    int i = rand() % v;
    while ( (int) l[i].size() == v)
      i = rand() % v;

    if ((int) _graph -> _list[i].size() < v)
    {
      int to = i;
      while (find(l[i].begin(), l[i].end(), to) != l[i].end())
        to = rand()%v;

      l[i].push_back(to);
      l[to].push_back(i);

      int cost = rand()%100;
      while (cost == 0)
        cost = rand()%100;
      _graph -> add_edge(i, to, cost);
      e += 2;
    }
  }
}

void Wrapper::build_random(int vertices, int d)
{
  edges = d*vertices*(vertices-1)/100;

  build_from_random(vertices,edges);
}

void Wrapper::build_from_file()
{
  cout << "Podaj nazwe pliku: ";
  string fileName;
  cin >> fileName;

  ifstream file(fileName);
  if(file.is_open())
  {
    parse_file(file);
  }
}

void Wrapper::save_to_file(string fileName)
{

  ofstream file(fileName);
  if(file.is_open())
  {
    save_file(file);
  }
}

void Wrapper::save_file(ofstream &file)
{
  file << vertices << " " << edges << endl;

  for(int i = 0; i< (int) _graph->_list.size(); i++ )
  {
    for(int j = 0; j<(int) _graph->_list[i].size(); j++)
      file << i << " " << _graph->_list[i][j].first << " " << _graph->_list[i][j].second << endl;
  }
}

void Wrapper::parse_file(ifstream &file)
{
  file >> vertices;
  file >> edges;
  init(vertices);

  int from,to,cost;
  while(!file.eof())
  {
    file >> from;
    file >> to;
    file >> cost;
   _graph -> add_edge(from, to, cost);
  }
}

void Wrapper::print_matrix()
{
  cout << "\tMACIERZ SASIEDZTWA\n\n\n";
  for(int i = 0; i< (int) _graph->_matrix.size(); i++)
  {
    cout << "\t" << i;
  }
  cout << endl << endl << endl;

  for(int i = 0; i< (int) _graph->_matrix.size(); i++)
  {
    cout << i;
    for(int j = 0; j< (int) _graph->_matrix[i].size(); j++)
    {
      cout << "\t";
      int val = _graph->_matrix[i][j];
      if(val != 0) cout << val;
      else cout << "-";
    }
    cout << endl << endl;
  }
}


void Wrapper::print_list()
{
  cout << "\tLISTA SASIEDZTWA\n\n\n";
  for(int i = 0; i< (int) _graph->_list.size(); i++ )
  {

    cout << i << " :";
    for(int j = 0; j<(int) _graph->_list[i].size(); j++)
    {
      cout << "\t" << (_graph->_list[i][j]).first
           << " - " << (_graph->_list[i][j]).second;
    }
    cout << endl << endl;
  }
}

void Wrapper::kruskal_l()
{
  vector<wedge> v = _graph->kruskal_l();
  //print_mst(v);
}

void Wrapper::kruskal_m()
{
  vector<wedge> v = _graph->kruskal_m();
 // print_mst(v);
}

void Wrapper::prim_l()
{
  vector<wedge> v = _graph->prim_l();
  //print_mst(v);

}

void Wrapper::prim_m()
{
  vector<wedge> v = _graph->prim_m();
  //print_mst(v);

}


void Wrapper::print_mst(std::vector<wedge> w)
{
  cout << "DRZEFFFFKO !!!" << endl << endl;
  for(auto &a : w)
  {
    cout << "krawedz: " << a.first.first << " - "
         << a.first.second << " waga: " << a.second
         << endl;
  }
}








