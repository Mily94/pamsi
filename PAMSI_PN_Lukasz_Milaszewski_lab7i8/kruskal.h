#ifndef KRUSKAL_H
#define KRUSKAL_H

#include <vector>

class Graph;

using edge = std::pair <int, int>;
using wedge = std::pair < edge, int>;

class Kruskal
{
public:
  Kruskal( Graph* _g);
  ~Kruskal();

  std::vector<int> root;
  std::vector<int> next;
  std::vector<int> len;

  void sum(wedge w);
  void perform_l();
  void perform_m();
  std::vector<wedge> tree;
  Graph* g;
};





#endif // KRUSKAL_H
