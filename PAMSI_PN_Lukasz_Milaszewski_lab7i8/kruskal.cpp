#include "kruskal.h"
#include "graph.h"
#include <queue>

Kruskal::Kruskal(Graph *_g)
{
  g = _g;
  int size = _g -> _matrix.size();

  for (int i = 0; i < size; i++)
  {
    root.push_back(i);
    next.push_back(i);
    len.push_back(1);

  }
}

Kruskal::~Kruskal()
{

}



class Comp
{
public:
  bool operator() (wedge e1, wedge e2)
  {
    return e1.second > e2.second;
  }
};

void Kruskal::perform_l()
{
  std::priority_queue<wedge,std::vector<wedge>, Comp> Q;

  for (int i = 0; i < (int) g->_list.size(); i++)
  {
    std::vector<vecpair> v = g-> _list[i];
    for (int j = 0; j < (int) v.size(); j++)
    {
      edge e = std::make_pair(i, v[j].first);
      wedge we = std::make_pair(e, v[j].second);

      Q.push(we);
    }
  }

  while(!Q.empty() && tree.size() < g->_list.size())
  {
    wedge w = Q.top();
    Q.pop();
    sum(w);
  }
}

void Kruskal::perform_m()
{
  std::priority_queue<wedge, std::vector<wedge>, Comp> Q;

  for (int i = 0; i < (int) g->_matrix.size(); i++)
  {
    std::vector<int> v = g->_matrix[i];
    for (int j = 0; j < (int) v.size(); j++)
    {
      if (v[j] != 0)
      {
        edge e = std::make_pair(i,j);
        wedge we = std::make_pair(e, v[j]);
        Q.push(we);
      }
    }
  }

  while(!Q.empty() && tree.size() < g->_matrix.size())
  {
    wedge w = Q.top();
    Q.pop();
    sum(w);
  }
}

void Kruskal::sum(wedge w)
{
  int v = w.first.first;
  int u = w.first.second;

  if (root[u] == root[v])
    return;
  else if (len[root[v]] < len[root[u]])
  {
    int rt = root[v];
    len[root[u]] += len[rt];
    root[rt] = root[u];
    for (int j = next[rt]; j != rt; j = next[j])
      root[j] = root[u];

    std::swap(next[rt], next[root[u]]);
    tree.push_back(w);
  }

  else if (len[root[v]] >= len[root[u]])
  {
    int rt = root[u];
    len[root[v]] += len[rt];
    root[rt] = root[v];
    for (int j = next[rt]; j != rt; j = next[j])
      root[j] = root[v];

    std::swap(next[rt], next[root[v]]);
    tree.push_back(w);
  }
}

