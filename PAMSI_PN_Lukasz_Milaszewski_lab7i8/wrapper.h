#ifndef WRAPPER_H
#define WRAPPER_H

#include "graph.h"
#include <fstream>

class Wrapper
{
public:
  Wrapper();
  ~Wrapper();

  int edges;
  int vertices;

  void init (int v);
  void build_from_file();
  void build_random(int v, int density);
  void build_from_random(int v, int edges);
  void print_list();
  void print_matrix();
  void save_file(std::ofstream &file);
  void save_to_file(std::string fileName);
  void parse_file(std::ifstream &file);
  void print_mst(std::vector<wedge> w);
  void kruskal_m();
  void kruskal_l();
  void prim_m();
  void prim_l();

  Graph *_graph;

};

#endif // WRAPPER_H
