#ifndef GRAPH_H
#define GRAPH_H

#include "prim.h"
#include "kruskal.h"



using vecpair = std::pair<int,int>;

using matrix = std::vector< std::vector<int> >;
using list = std::vector< std:: vector <vecpair> >;

class Graph
{
public:
  Graph();
  ~Graph();

  std::vector<wedge> kruskal_m();
  std::vector<wedge> kruskal_l();
  std::vector<wedge> prim_m();
  std::vector<wedge> prim_l();

  void add_edge(int from, int to, int cost);

private:

  matrix _matrix;
  list _list;

  friend class Kruskal;
  friend class Wrapper;
  friend class Prim;

};


#endif // GRAPH_H
