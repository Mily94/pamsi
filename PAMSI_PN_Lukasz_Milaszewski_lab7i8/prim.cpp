#include "graph.h"
#include "prim.h"

#include <queue>

using namespace std;

class Comp
{
public:
  bool operator()(wedge e1, wedge e2)
  {
    return e1.second > e2.second;
  }
};


void Prim::perform_m()
{
  Graph* graph = g;
  int size = graph->_matrix.size();
  vector<bool> visited(size);

  for(int i = 0; i<size; i++)
  {
    visited[i] = false;
  }
  priority_queue<wedge, std::vector<wedge>, Comp> Q;

  int v = 0;
  visited[v] = true;

  for(int i = 1; i<size;i++)
  {
    vector<int> vec = g-> _matrix[v];
    for(int j = 0; j< (int) vec.size(); j++)
    {
      if( vec[j] != 0)
      {
        if(!visited[j])
        {
          edge e = std::make_pair(v, j);
          wedge we = std::make_pair(e, vec[j]);
          Q.push(we);
        }
      }
    }

    wedge w;
    do
    {
      if(!Q.empty())
      {
        w = Q.top();
        Q.pop();
      }
    }while( visited[ w.first.second] && !Q.empty());

    tree.push_back(w);
    visited[ w.first.second ] = true;
    v = w.first.second;
  }
}



void Prim::perform_l()
{
  int size = g->_list.size();
  vector<bool> visited(size);

  for(int i = 0; i<size; i++)
      visited[i] = false;

  priority_queue<wedge, std::vector<wedge>, Comp> Q;

  int v = 0;
  visited[v] = true;

  for(int i = 1; i<size; i++)
  {
    vector<vecpair> vec =g-> _list[v];
    for(int j = 0; j < (int) vec.size(); j++)
    {
      if(!visited[ vec[j].first ])
      {
        edge e = std::make_pair(v, vec[j].first);
        wedge we = std::make_pair(e, vec[j].second);

        Q.push(we);
      }
    }

    wedge w;
    do
    {
      if(!Q.empty())
      {
        w = Q.top();
        Q.pop();
      }
    }while( visited[w.first.second] && !Q.empty());
    tree.push_back(w);

    visited[ w.first.second ] = true;
    v = w.first.second;
  }
}
