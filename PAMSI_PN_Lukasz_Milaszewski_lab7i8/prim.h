#ifndef PRIM_H
#define PRIM_H

#include <vector>

class Graph;
using edge = std::pair<int,int>;
using wedge = std::pair<edge,int>;

class Prim
{
public:

  Prim(Graph* g): g(g) {}

  void perform_m();
  void perform_l();
  std::vector<wedge> tree;
   Graph* g;

};

#endif // PRIM_H
