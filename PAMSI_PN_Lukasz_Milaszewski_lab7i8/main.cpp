#include "graph.h"
#include "kruskal.h"
#include "wrapper.h"

#include <windows.h>
#include <iostream>

using namespace std;

void Timer(int vertices, int density);

int main()
{
  Timer(100, 100);
  Timer(100, 75);
  Timer(100, 50);
  Timer(100, 25);

  return 0;
}



void Timer(int vertices, int density)
{

  LARGE_INTEGER frequency;        // ticks per second
  LARGE_INTEGER t1, t2;           // ticks
  double elapsedTimeKL, elapsedTimeKM;
  double elapsedTimePL, elapsedTimePM;
  double elapsedTime;

  QueryPerformanceFrequency(&frequency);

  elapsedTimeKL = 0;
  elapsedTimeKM = 0;
  elapsedTimePL = 0;
  elapsedTimePM = 0;

  Wrapper p;
 // int counter = 1;
  for (int i = 0; i < 100; i++)
  {
   // string d = to_string(density);
   // string v = to_string(vertices);
   // string c = to_string(counter);



    p.build_random(vertices,density);

    QueryPerformanceCounter(&t1);
    p.kruskal_l();
    QueryPerformanceCounter(&t2);
    elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
    //cout <<"kurskal_list: " << elapsedTime << " ms.\n" << endl;
    elapsedTimeKL += elapsedTime;

    QueryPerformanceCounter(&t1);
    p.kruskal_m();
    QueryPerformanceCounter(&t2);
    elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
    //cout <<"kurskal_matrix: " << elapsedTime << " ms.\n" << endl;
    elapsedTimeKM += elapsedTime;

    QueryPerformanceCounter(&t1);
    p.prim_l();
    QueryPerformanceCounter(&t2);
    elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
    //cout <<"prim_list: " << elapsedTime << " ms.\n" << endl;
    elapsedTimePL += elapsedTime;

    QueryPerformanceCounter(&t1);
    p.prim_m();
    QueryPerformanceCounter(&t2);
    elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
    //cout <<"prim_matrix: " << elapsedTime << " ms.\n" << endl;
    elapsedTimePM += elapsedTime;

    //string fileName = "vertices" + v + "_" + d + "_" + c + ".txt";
    //counter ++;
    //p.build_random(vertices,density);
    //p.save_to_file(fileName);

  }
  cout << "vertices: " << vertices << "  density: " << density << endl << endl;
  cout << "kruskal_list: " << elapsedTimeKL/100 << endl;
  cout << "kruskal_matrix: " << elapsedTimeKM/100 << endl;
  cout << "prim_list: " << elapsedTimePL/100 << endl;
  cout << "prim_matrix: " << elapsedTimePM/100 << endl;
  cout << "-------------------------------------------" << endl;
}
