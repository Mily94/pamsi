#include "graph.h"
#include "kruskal.h"

#include <iostream>
#include <queue>

using edge = std::pair<int, int>;
using wedge = std::pair<edge, int>;

using namespace std;

Graph::Graph()
{

}

Graph::~Graph()
{

}

std::vector<wedge> Graph::prim_l()
{
  Prim p(this);
  p.perform_l();

  vector<wedge> v = p.tree;

  return v;
}

std::vector<wedge> Graph::prim_m()
{
  Prim p(this);
  p.perform_m();

  vector<wedge> v = p.tree;

  return v;
}




std::vector<wedge> Graph::kruskal_l()
{
  Kruskal k(this);
  k.perform_l();

  vector<wedge> v = k.tree;

  return v;
}

std::vector<wedge> Graph::kruskal_m()
{
  Kruskal k(this);
  k.perform_m();

  vector<wedge> v = k.tree;

  return v;
}



void Graph::add_edge(int from, int to, int cost)
{
  _matrix[from][to] = cost;
  _matrix[to][from] = cost;

  _list[from].push_back(make_pair(to,cost));
  _list[to].push_back(make_pair(from,cost));
}



