#include "othello.h"

void Othello::startGame(int player) {

    int maxDepth = 3;
    int startDepth = 0;

    auto board = initializeBoard();
    makeStartingBoard(board);
    printBoard(board);

    while ( !isBoardFull(board) ) {

        initializeTree(board);
        auto root = getTreeRoot();
        board = getBoardAfterPlayerMove(player, root);
        printBoard(board);

        cout << "PLANSZA PO RUCHU PRZECIWNIKA" << endl;
        initializeTree(board);
        root = getTreeRoot();
        buildTree(getOpponent(player), root, startDepth, maxDepth);
        board = getBoardAfterMove(getOpponent(player), root, maxDepth);
        printBoard(board);
    }

}

vector<vector<int>> Othello::initializeBoard() {

    auto board = vector <vector<int>> (10, vector<int>(10));
    for (int i = 0; i < 10; i++)
        for (int j = 0; j < 10; j++)
            board[i][j] = OUT;

    for (int i = 1; i < 9; i++)
        for (int j = 1; j < 9; j++)
            board[i][j] = EMPTY;

    return board;
}

void Othello::makeStartingBoard(vector<vector<int>>& board) {

    board[4][4] = WHITE;
    board[5][5] = WHITE;
    board[5][4] = BLACK;
    board[4][5] = BLACK;
}

void Othello::printBoard(vector<vector<int>> board) {

    for (int i = 1; i < 9; i++) {
        for (int j = 1; j < 9; j++) {
            if (board[i][j] == EMPTY)
                cout << "+" << " ";
            if (board[i][j] == WHITE)
                cout << "X" << " ";
            if (board[i][j] == BLACK)
                cout << "O" << " ";
        }
        cout << endl;
    }
    cout << endl;
}

void Othello::initializeTree(vector<vector<int>> board) {

    tree = Tree(NodePtr(new Node(board)));
}

NodePtr Othello::getTreeRoot() {
    return tree.getHead();
}

vector<vector<int>> Othello::getBoardAfterPlayerMove(int player, NodePtr root) {

    int i,j;
    bool temp = true;
    vector<vector<int>> next_board;
    while (temp) {
        cout << "Podaj wspolrzedne: ";
        cin >> j >> i;
        cout << endl;
        if (isFieldEmpty(i,j,root) && isPossibleMove(i,j,player,root)) {

            next_board = root ->getTable();
            makeMove(i,j,player,next_board);
            temp = false;
            cout << "PLANSZA PO TWOIM RUCHU" << endl;
        }
        else
            cout << "Ruch niedozwolony! Podaj inne wspolrzedne. " << endl;
    }
    return next_board;
}

void Othello::performMove(int player, vector<vector<int>>& board, int i, int posX, int j, int posY) {

    int opponent = getOpponent(player);
    int x,y;
    x = i;
    y = j;
    if (board[i+posX][j+posY] == opponent) {
       while (board[i+posX][j+posY] == opponent) {
           i = i + posX;
           j = j + posY;
       }
       if (board[i+posX][j+posY] == player ) {
           board[x][y] = player;
           while (board[x+posX][y+posY] == opponent) {
                board[x+posX][y+posY] = player;
                x = x + posX;
                y = y + posY;
           }
       }
   }
}

void Othello::makeMove(int i, int j, int player, vector<vector<int>>& board) {

    // 1 statement
    performMove(player,board,i,-1,j,-1);


    // 2 statement
    performMove(player,board,i,0,j,-1);

    // 3 statement
    performMove(player,board,i,1,j,-1);

    // 4 statement
    performMove(player,board,i,-1,j,0);


    // 5 statement
    performMove(player,board,i,1,j,0);


    // 6 statement
    performMove(player,board,i,-1,j,1);

    // 7 statement
    performMove(player,board,i,0,j,1);

    // 8 statement
    performMove(player,board,i,1,j,1);
}

bool Othello::isFieldEmpty(int i, int j, NodePtr node) {

    return node->getTable()[i][j] == EMPTY;
}

int Othello::getOpponent(int player) {

    if (player == BLACK)
        return WHITE;
    return BLACK;
}

void Othello::buildTree(int player, NodePtr node, int depth, int maxDepth) {

    if (depth > maxDepth+1) return;

    for (int i = 1; i < 9; i++) {
        for (int j = 1; j < 9; j++) {
            if(isPossibleMove(i,j,player, node)) {
                auto next_board = node->getTable();
                makeMove(i,j,player,next_board);
                auto childNode = new Node(next_board);
                childNode ->move.setPosX(i);
                childNode ->move.setPosY(j);
                node->addChild(NodePtr(childNode));
            }
        }
    }

    for (auto NodeChild : node->getChildren())
        buildTree(getOpponent(player), NodeChild, depth+1, maxDepth);

    auto next_board = node->getTable();
    if (node ->getChildren().empty() && !isBoardFull(next_board)) {
        auto childNode = new Node(next_board);
        node->addChild(NodePtr(childNode));
        buildTree(getOpponent(player), NodePtr(childNode), depth +1,maxDepth);
    }
}

bool Othello::isBoardFull(vector<vector<int>> tab) {

    for (int i = 1; i < 9; i++)
    {
        for (int j = 1; j < 9; j++)
        {
            if (tab[i][j] == EMPTY ) return false;
        }
    }
    return true;
}

bool Othello::checkStatement(vector<vector<int>> board, int player,int i, int posX, int j, int posY) {

    int opponent = getOpponent(player);
    if (board[i+posX][j+posY] == opponent) {
        while (board[i+posX][j+posY] == opponent) {
            i = i + posX;
            j = j + posY;
        }
        if (board[i+posX][j+posY] == player)
            return true;
    }
    return false;
}

bool Othello::isPossibleMove(int i, int j, int player, NodePtr node) {
    if (!isFieldEmpty(i,j, node)) return false;

    auto board = node->getTable();

    // 1 statement
    if ( checkStatement(board,player,i,-1,j,-1))
        return true;

    // 2 statement
    if ( checkStatement(board,player,i,0,j,-1))
        return true;

   // 3 statement
    if ( checkStatement(board,player,i,1,j,-1))
        return true;

   // 4 statement
    if ( checkStatement(board,player,i,-1,j,0))
        return true;

   // 5 statement
    if ( checkStatement(board,player,i,1,j,0))
        return true;

   // 6 statement
    if ( checkStatement(board,player,i,-1,j,1))
        return true;

   // 7 statement
    if ( checkStatement(board,player,i,0,j,1))
        return true;

   // 8 statement
    if ( checkStatement(board,player,i,1,j,1))
        return true;

   return false;
}

int Othello::count(int player, NodePtr node) {

    int counter = 0;
    auto board = node -> getTable();
    for (int i = 1; i < 9; i++) {
        for (int j = 1; j < 9; j++) {
            if (board[i][j] == player)
                counter++;
        }
    }
    return counter;
}

int Othello::MiniMax(int player, NodePtr node, bool isMax, int depth, int maxDepth) {

    if (depth > maxDepth || node->isLeaf()) {
        int wynik = count(player,node);
        return wynik;
    }
    int best = isMax ? -99 : 99;

    for (auto childNode : node -> getChildren() ) {
        int temp = MiniMax(getOpponent(player), childNode, !isMax, depth+1, maxDepth);
        best = isMax ? max(best, temp) : min(best, temp);
    }

    return best;
}

bool Node::isLeaf() {

    return children.empty();

}

vector<vector<int>> Othello::getBoardAfterMove(int player, NodePtr node, int maxDepth)
{
    int index= -99;
    int temp;
    int best = -2000;
    for (int i = 0; i < (int) node->getChildren().size(); i++) {
        temp = MiniMax(player, NodePtr(node->getChildren()[i]), true, 0,maxDepth);
        if (temp > best) {
            best = temp;
            index = i;
        }
    }

    return index != -99 ? node->getChildren()[index]->getTable() : node->getTable();
}
