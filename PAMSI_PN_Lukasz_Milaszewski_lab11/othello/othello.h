#ifndef OTHELLO_H
#define OTHELLO_H
#include <iostream>
#include <vector>
#include <cstdlib>
#include <cstdio>
#include <memory>
#include <limits>
#include <cmath>

using namespace std;

class Node;
using NodePtr = std::shared_ptr<Node>;

class Move {

public:
    int getPosX() { return posX; }
    int getPosY() { return posY; }
    void setPosX(int x) { posX = x; }
    void setPosY(int y) { posY = y; }

private:
    int posX;
    int posY;
};

class Node {
public:
    Move move;
    Node(vector<vector<int>> table) : table(table) {}
    void addChild(NodePtr child) { children.push_back(child); }
    vector<NodePtr> getChildren() { return children; }
    vector<vector<int>> getTable() { return table; }
    bool isLeaf();

private:
    vector<vector<int>> table;
    vector<NodePtr> children;

};

class Tree {
public:
    Tree() : head(nullptr) {}
    Tree(NodePtr head) : head(head) {}
    NodePtr getHead() { return head; }

private:
    NodePtr head;
};

class Othello {

public:
    const int WHITE = 1;
    const int BLACK = 2;
    const int OUT = 9;
    const int EMPTY = 0;

    void startGame(int player);
    vector<vector<int>> initializeBoard();
    void makeStartingBoard(vector<vector<int>>& board);
    void printBoard(vector<vector<int>> board);
    void initializeTree(vector<vector<int>> board);
    NodePtr getTreeRoot();
    vector<vector<int>> getBoardAfterPlayerMove(int player, NodePtr root);
    bool isFieldEmpty(int i, int j, NodePtr node);
    int getOpponent(int player);
    bool isPossibleMove(int i, int j, int player, NodePtr node);
    void makeMove(int i, int j, int player, vector<vector<int>> & board);
    bool isBoardFull(vector<vector<int>> tab);
    vector<vector<int>> getBoardAfterMove(int player, NodePtr node, int maxDepth);
    int MiniMax(int player, NodePtr node, bool isMax, int depth, int maxDepth);
    int count(int player, NodePtr node);
    void buildTree(int player, NodePtr node, int depth, int maxDepth);
    bool checkStatement(vector<vector<int>> board, int player, int i, int posX, int posY, int j);
    void performMove(int player, vector<vector<int>>& board, int i, int posX, int j, int posY);

    Tree tree;
};

#endif // OTHELLO_H
