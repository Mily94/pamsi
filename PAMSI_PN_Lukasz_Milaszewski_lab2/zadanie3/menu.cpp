// Lukasz Milaszewski 209385 pn: 8:15

#include "menu.h"
#include "queue.h"

using namespace std;

void menu::displayMenu()
{
  queue q;
  int option;
  do
  {
    cout << "1.Dodaj element" << endl;
    cout << "2.Usun element" << endl;
    cout << "3.Usun kolejke" << endl;
    cout << "4.Wyswietl kolejke" << endl;
    cout << "0.Koniec " << endl;
    cout << "Wybierz opcje: ";
    cin >> option;
    switch(option)
    {
      case 1:
        q.insertElement();
        break;
      case 2:
        q.deleteElement();
        break;
      case 3:
        q.deleteQueue();
        break;
      case 4:
        q.display();
        break;
    }
  } while (option != 0);
}
