// Lukasz Milaszewski 209385 pn: 8:15

#ifndef QUEUE_H
#define QUEUE_H

#include "node.h"
#include <iostream>

class queue
{
  node * head;
  node * tail;
public:
  queue();
  node* createNode(double);
  void insertElement();
  void deleteElement();
  void deleteQueue();
  void display();
  ~queue();
};

#endif // QUEUE_H
