// Lukasz Milaszewski 209385 pn: 8:15

#include "queue.h"

using namespace std;

queue::queue()
{
  head = NULL;
  tail = NULL;
}

node *queue::createNode(double value)
{
  node *temp;
  temp = new(node);
  temp->data = value;
  temp->next = NULL;
  return temp;
}

void queue::insertElement()
{
  node *temp;
  double element;
  cout << "-----------------------" << endl;
  cout << "Podaj element: ";
  cin >> element;
  cout << "-----------------------" << endl;
  if (tail == NULL)
  {
    tail = new node;
    tail -> data = element;
    tail -> next = NULL;
    //tail -> prev = NULL;
    head = tail;

  } else
  {
    temp = new node;
    temp -> data = element;
    temp -> next = NULL;
   // temp -> prev = tail;
    tail -> next = temp;
    tail = temp;
  }
}



void queue::deleteElement()
{
  if (head != NULL)
  {
    node *temp;
    temp = head;
    head = head -> next;
    delete temp;
    cout << "------------------------" << endl;
    cout << "Pierwszy element zostal usuniety" << endl;
    cout << "------------------------" << endl;
  } else
  {
    cout << "------------------------" << endl;
    cout << "Nie mozna usunac, poniewaz kolejka jest pusta" << endl;
    cout << "------------------------" << endl;
  }
}

void queue::display()
{
  node *temp;
  if (head == NULL)
  {
    cout << "------------------------" << endl;
    cout << "Kolejka jest pusta" << endl;
    cout << "------------------------" << endl;
    return;
  }
  temp = head;
  cout << "------------------------" << endl;
  cout << "Elementy listy: " << endl;
  while (temp != NULL)
  {
    cout << temp->data << " ";
    temp = temp->next;
  }
  cout << endl;
  cout << "------------------------" << endl;
}

void queue::deleteQueue ()
{
  node *temp;
  while (head != NULL)
  {
    temp = head;
    head = head -> next;
    delete temp;
  }
  cout << "------------------------" << endl;
  cout << "Kolejka zostala usunieta." << endl;
  cout << "------------------------" << endl;
}

queue::~queue()
{
  deleteQueue();
}
