// Lukasz Milaszewski 209385 pn: 8:15

#ifndef NODE_H
#define NODE_H

class node
{
public:
    double data;
    node *next;
};

#endif // NODE_H
