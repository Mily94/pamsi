#ifndef NODE_H
#define NODE_H

#include <string>

using namespace std;

class node
{
public:
  string data;
  node *next;
  node *prev;
};

#endif // NODE_H
