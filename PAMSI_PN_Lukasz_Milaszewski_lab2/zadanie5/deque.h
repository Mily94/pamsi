#ifndef DEQUE_H
#define DEQUE_H

#include "node.h"
#include <iostream>
#include <string>

class Deque
{
  node *head,*tail;
  int palIter; // zmienna potrzebna do funkcjonowania sprawdzPalindrom()
  int T; // dlugosc badanego wyrazu
  int tempT; // kopia dlugosci badanego wyrazu

public:

  Deque();
  void wstawPierwszy();
  void wyswietl();
  void usunPierwszy();
  void usunOstatni();
  void wstawOstatni();
  void wyswietlPierwszy();
  void wyswietlOstatni();
  void usunKolejke();
  void sprPierwszy();
  void sprOstatni();
  void sprWszystkie();
  bool jestPal(string);
  void sprawdzPalindrom();
  ~Deque();

};

#endif // DEQUE_H
