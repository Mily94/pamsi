#include "menu.h"
#include "deque.h"

using namespace std;

void menu::displayMenu()
{
  Deque q;
  int option;
  do
  {
    cout << "1.Wstaw element na poczatek" << endl;
    cout << "2.Wstaw element na koniec" << endl;
    cout << "3.Usun element na poczatku" << endl;
    cout << "4.Usun element na koncu" << endl;
    cout << "5.Wyswietl pierwszy element" << endl;
    cout << "6.Wyswietl ostatni element" << endl;
    cout << "7.Wyswietl kolejke" << endl;
    cout << "8.Sprawdz czy palindrom" << endl;
    cout << "9.Usun kolejke" << endl;
    cout << "0.Koniec " << endl;
    cout << "Wybierz opcje: ";
    cin >> option;
    switch(option)
    {
      case 1:
        q.wstawPierwszy();
        break;
      case 2:
        q.wstawOstatni();
        break;
      case 3:
        q.usunPierwszy();
        break;
      case 4:
        q.usunOstatni();
        break;
      case 5:
        q.wyswietlPierwszy();
        break;
      case 6:
        q.wyswietlOstatni();
        break;
      case 7:
        q.wyswietl();
        break;
      case 8:
        q.sprawdzPalindrom();
        break;
      case 9:
        q.usunKolejke();
        break;
    }
  } while (option != 0);
}



