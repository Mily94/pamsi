#include "deque.h"

using namespace std;

Deque::Deque()
{
  T = 0;
  tempT = 0;
  head = NULL;
  tail = NULL;
}

void Deque::wstawPierwszy()
{
  node *temp;
  string element;
  cout << "-----------------------" << endl;
  cout << "Podaj element: " ;
  cin >> element;
  cout << "-----------------------" << endl;

  if( head == NULL)
  {
    head = new node;
    head -> data = element;
    head -> next = NULL;
    head -> prev = NULL;
    tail = head;
  } else
  {
    temp = new node;
    temp -> data = element;
    temp -> next = head;
    temp -> prev = NULL;
    head -> prev = temp;
    head = temp;
  }
}

void Deque::wstawOstatni()
{
  node *temp;
  string element;
  cout << "-----------------------" << endl;
  cout << "Podaj element: ";
  cin >> element;
  cout << "-----------------------" << endl;
  if (tail == NULL)
  {
    tail = new node;
    tail -> data = element;
    tail -> next = NULL;
    tail -> prev = NULL;
    head = tail;

  } else
  {
    temp = new node;
    temp -> data = element;
    temp -> next = NULL;
    temp -> prev = tail;
    tail -> next = temp;
    tail = temp;
  }
}

void Deque::usunPierwszy()
{
  if (head != NULL)
  {
    if (head -> next == NULL)
    {
      node *temp;
      temp = head;
      head = NULL;
      delete temp;
    } else
    {
    node *temp;
    temp = head;
    head = head -> next;
    head -> prev = NULL;
    delete temp;
    }
    cout << "-----------------------" << endl;
    cout << "Element usunieto." << endl;
    cout << "-----------------------" << endl;
   } else
    {
      cout << "-----------------------" << endl;
      cout << "Nie mozna usunac, poniewaz nie ma elementow";
      cout << "-----------------------" << endl;
    }
}

void Deque::usunOstatni()
{
  if (tail != NULL)
  {
    if (tail -> prev == NULL)
    {
      node *temp;
      temp = tail;
      tail = NULL;
      head = NULL;
      delete temp;
    } else
    {
    node *temp;
    temp = tail;
    tail = tail -> prev;
    tail -> next = NULL;
    delete temp;
    }
    cout << "-----------------------" << endl;
    cout << "Element usunieto." << endl;
    cout << "-----------------------" << endl;
  } else
  {
    cout << "-----------------------" << endl;
    cout << "Nie mozna usunac, poniewaz nie ma elementow";
    cout << "-----------------------" << endl;
  }
}

void Deque::wyswietl()
{
  node *temp;
  temp = head;
  cout << "-----------------------" << endl;
  while(temp != NULL)
  {
    cout << temp -> data <<" ";
    temp = temp -> next;
  }
  cout << endl;
  cout << "-----------------------" << endl;
}

void Deque::wyswietlPierwszy()
{
  cout << "-----------------------" << endl;
  cout << head -> data << endl;
  cout << "-----------------------" << endl;
}

void Deque::wyswietlOstatni()
{
  cout << "-----------------------" << endl;
  cout << tail -> data << endl;
  cout << "-----------------------" << endl;
}

void Deque::usunKolejke()
{
  node *temp;
  while (head != NULL)
  {
    temp = head;
    head = head -> next;
    delete temp;
  }

  node *temp2;
  temp2 = tail;
  tail = tail -> prev;
  delete temp2;
  //while (tail != NULL)
  //{
   // temp2 = tail;
   // tail = tail -> prev;
   // delete temp2;
 //}

  cout << "------------------------" << endl;
  cout << "Kolejka zostala usunieta." << endl;
  cout << "------------------------" << endl;
}
void Deque::sprawdzPalindrom()
{
  int opt;
  cout << "-----------------------" << endl;
  cout << "1.Sprawdz pierwszy element" << endl;
  cout << "2.Sprawdz ostatni element" << endl;
  cout << "3.Sprawdz wszystkie elementy" << endl;
  cout << "Wybierz opcje: ";
  cin >> opt;
  if (opt == 1)
    sprPierwszy();
  if (opt == 2)
    sprOstatni();
  if (opt == 3)
    sprWszystkie();
  cout << "-----------------------" << endl;
}

void Deque::sprPierwszy()
{
  if (head != NULL)
  {
    T = head -> data.length();
    tempT = T;
    palIter = 0;
    if (jestPal(head -> data))
    {
      cout << "-----------------------" << endl;
      cout << head -> data << " : Jest palindromem." << endl;
      cout << "-----------------------" << endl;
    } else
    {
      cout << "-----------------------" << endl;
      cout << head -> data << " : Nie jest palindromem." << endl;
      cout << "-----------------------" << endl;
    }
  }
  else
  {
    cout << "-----------------------" << endl;
    cout << "Kolejka nie posiada elementow." << endl;
    cout << "-----------------------" << endl;
  }
}

void Deque::sprOstatni()
{
  if (tail != NULL)
  {
    T = tail -> data.length();
    tempT = T;
    palIter = 0;
    if (jestPal(tail -> data))
    {
      cout << "-----------------------" << endl;
      cout << tail -> data << " : Jest palindromem." << endl;
      cout << "-----------------------" << endl;
    }
      else
    {
      cout << "-----------------------" << endl;
      cout << tail -> data << " : Nie jest palindromem." << endl;
      cout << "-----------------------" << endl;
    }
  }
  else
  {
    cout << "-----------------------" << endl;
    cout << "Kolejka nie posiada elementow." << endl;
    cout << "-----------------------" << endl;
  }
}

void Deque::sprWszystkie()
{
  node *temp;
  if (head != NULL)
  {
    temp = head;
    while (temp != NULL)
    {
      T = temp -> data.length();
      tempT = T;
      palIter = 0;
      cout << "-----------------------" << endl;
      cout << temp -> data << " : ";
      if (jestPal(temp -> data))
      {
        cout << "Jest palindromem." << endl;
        cout << "-----------------------" << endl;
      }else
      {
        cout << "Nie jest palindromem." << endl;
        cout << "-----------------------" << endl;
        temp = temp -> next;
      }
    }
  } else
  {
    cout << "-----------------------" << endl;
    cout << "Kolejka jest pusta." << endl;
    cout << "-----------------------" << endl;
  }
}

bool Deque::jestPal(string testStr)
{
  if(tempT < 2) return true;
  if( testStr[palIter] == testStr[T -1 -palIter])
  {
    tempT = tempT - 2;
    palIter++;
    return jestPal(testStr);
  }
  else
  {
    return false;
  }
}

Deque::~Deque()
{
  usunKolejke();
}
