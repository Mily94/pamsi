//       Lukasz Milaszewski 209385  poniedzialek 8:15


#include "permutation.h"

using namespace std;

void Permutation::displayMenu()
{
  int option;
  do
  {
    cout << "1.Podaj wyraz " << endl;
    cout << "2.Wyswietl wszystkie permutacje" << endl;
    cout << "3.Wyswietl wszystkie palindromy" << endl;
    cout << "4.Usun duplikaty palindromow" << endl;
    cout << "0.Koniec" << endl;
    cout << "Wybierz opcje: ";
    cin >> option;

    switch(option)
    {
      case 1:
        setWord();
        break;

      case 2:
        displayPerm();
        break;

      case 3:
        displayPal();
        break;
      case 4:
        usunDup();
        break;
    }
  }   while (option != 0);
}

void Permutation::setWord()
{
  cout << "------------------------" << endl;
  cin >> word;
  resetValues();
  T = word.length();
  tempT = T;
  makeContainer(silnia(T));
  perm(word,0,word.length()-1);
  cout << "------------------------" << endl;
}

void Permutation::makeContainer(int k)
{
  if(arr)
    delete [] arr;

  if(arrPal)
    delete [] arrPal;

  if(arrTemp)
    delete [] arrTemp;


  arr = new string[k];
  arrPal = new string[k];
}

void Permutation::perm(string d,int i, int n)
{
  int j;
    if (i == n)
    {
      arr[allPerm] = d;
      allPerm++;
      tempT = T;
      palIter = 0;
     if (jestPal(d))
       {
        arrPal[permIter] = d;
        permIter++;
      }
    }
    else
    {
      for (j = i; j < n+1; j++)
      {
        swap(d[i],d[j]);
        perm(d, i + 1, n);
        swap(d[i],d[j]);
      }
    }
}

void Permutation::swap(char& a , char& b)
{
    char temp;
    temp = a;
    a = b;
    b = temp;
}

int Permutation::silnia(int a)
{
  if (a == 0)
    return 1;
  else
    if (a == 1)
      return 1;
    else
      return a*silnia(a-1);
}

bool Permutation::jestPal(string testStr)
{
   if(tempT < 2) return true;
   if( testStr[palIter] == testStr[T -1 -palIter])
   {
     tempT = tempT - 2;
     palIter++;
     return jestPal(testStr);
   }
   else
   {
     return false;
   }
}

void Permutation::resetValues()
{
  allPerm = 0;
  palIter = 0;
  permIter = 0;
  T = 0;
  tempT = 0;
}

void Permutation::displayPerm()
{
  int iter = silnia(T);
  cout << "------------------------" << endl;
  for (int i = 0; i < iter; i++)
    cout << arr[i] << endl;
  cout << "Ilosc permutacji: " << iter << endl;
  cout << "------------------------" << endl;
}

void Permutation::displayPal()
{
  cout << "------------------------" << endl;
  for (int i = 0; i < permIter; i++ )
    cout << arrPal[i] << endl;
  cout << "Ilosc palindromow: " << permIter << endl;
  cout << "------------------------" << endl;
}

void Permutation::usunDup()
{
  arrTemp = new string[permIter];
  int count = 0;
  int j = 0;

  for (int i = 0; i < permIter; i++)
  {
    arrTemp[i] = arrPal[i];
  }

  for(int i = 0; i < permIter; i++)
  {
    for(j = 0; j < count; j++)
    {
      if(arrTemp[i] == arrPal[j])
        break;
    }
    if(j == count)
    {
      arrPal[count] = arrTemp[i];
      count++;
    }
  }
  cout << "------------------------" << endl;
  cout << "Usunieto " << permIter - count << "  duplikaty/ow !" << endl;
  cout << "------------------------" << endl;
  permIter = count;
}


Permutation::~Permutation()
{
  delete [] arr;
  delete [] arrPal;
  delete [] arrTemp;
}
