//       Lukasz Milaszewski 209385  poniedzialek 8:15

#ifndef permutation_H
#define permutation_H

#include <iostream>
#include <string>

using namespace std;

class Permutation
{
  string *arr = nullptr;       // kontener permutacji
  string *arrPal = nullptr;    // kontener palindromow
  string *arrTemp = nullptr;   // tablica potrzebna przy usuwaniu duplikatow

  string word;
  int T;            // dlugosc slowa
  int tempT;        // dlugosc slowa do poprawnego dzialania funkcji jestPal
  int permIter;     // iterator palindromow
  int palIter;      // iterator funkcji jestPal
  int allPerm;      // iterator wszystkich permutacji

public:
  ~Permutation();
  void setWord();
  void makeContainer(int);
  void displayMenu();
  void swap(char&,char&);
  void perm(string, int, int);
  int silnia(int);
  bool jestPal(string);
  void displayPerm();
  void displayPal();
  void resetValues(); // funkcja zeruje wartosci zmiennych
  void usunDup();
};

#endif // PERMUTATION_H
