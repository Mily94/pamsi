#include "linkedList.h"

using namespace std;

linkedList::linkedList()
{
  head = NULL;
}

node *linkedList::createNode(int value)
{
  node *temp;
  temp = new(node);
  temp->data = value;
  temp->next = NULL;
  return temp;
}

void linkedList::insertBegin()
{
  int value;
  cout << "------------------------" << endl;
  cout << "Podaj wartosc: ";
  cin >> value;
  node *temp, *p;
  temp = createNode(value);
  if (head == NULL)
  {
    head = temp;
    head->next = NULL;
  }
  else
  {
    p = head;
    head = temp;
    head->next = p;
  }
  cout << "Element zostal dodany." << endl;
  cout << "------------------------" << endl;
}

void linkedList::insertLast()
{
  int value;
  cout << "------------------------" << endl;
  cout << "Podaj wartosc: ";
  cin >> value;
  node *temp, *s;
  temp = createNode(value);
  if (head == NULL)
  {
    head = temp;
    head->next = NULL;
  } else
  {
    s = head;
    while (s->next != NULL)
    {
      s = s->next;
    }
    temp->next = NULL;
    s->next = temp;
  }
  cout << "Element zostal dodany." << endl;
  cout << "------------------------" << endl;
}

void linkedList::insertPos()
{
  int value, pos, counter = 0;
  cout << "------------------------" << endl;
  cout << "Podaj wartosc: ";
  cin >> value;
  node *temp, *s, *ptr;
  temp = createNode(value);
  cout << "Podaj pozycje: ";
  cin >> pos;
  cout << "------------------------" << endl;
  int i;
  s = head;
  while (s != NULL)
  {
    s = s->next;
    counter++;
  }
  if (pos == 1)
  {
    if (head == NULL)
    {
      head = temp;
      head->next = NULL;
    }
    else
    {
      ptr = head;
      head = temp;
      head->next = ptr;
    }
  }
  else if (pos > 1  && pos <= counter)
  {
    s = head;
    for (i = 1; i < pos; i++)
    {
      ptr = s;
      s = s->next;
    }
    ptr->next = temp;
    temp->next = s;
  }
  else
  {
    cout << "------------------------" << endl;
    cout << "Nie ma takiej pozycji. Podaj wartosc z przedzialu 1 - " << counter << endl;
    cout << "------------------------" << endl;
  }
    cout << "Element zostal dodany." << endl;
    cout << "------------------------" << endl;
}

void linkedList::deletePos()
{
  int pos, i, counter = 0;
  if (head == NULL)
  {
    cout << "------------------------" << endl;
    cout << "Lista jest pusta" << endl;
    cout << "------------------------" << endl;
    return;
  }
  cout << "------------------------" << endl;
  cout << "Podaj pozycje: ";
  cin >> pos;
  cout << "------------------------" << endl;
  node *s, *ptr;
  s = head;
  if (pos == 1)
  {
    head = s->next;
  }
  else
  {
    while (s != NULL)
    {
      s = s->next;
      counter++;
    }
    if (pos > 0 && pos <= counter)
    {
      s = head;
      for (i = 1;i < pos;i++)
      {
        ptr = s;
        s = s->next;
      }
      ptr->next = s->next;
    }
    else
    {
      cout << "Nie ma takiej pozycji. Podaj wartosc z przedzialu 1 - " << counter << endl;
      cout << "------------------------" << endl;
    }
    delete s;
    cout << "Element zostal usuniety." << endl;
    cout << "------------------------" << endl;
  }
}

void linkedList::display()
{
  node *temp;
  if (head == NULL)
  {
    cout << "------------------------" << endl;
    cout << "Lista jest pusta" << endl;
    cout << "------------------------" << endl;
    return;
  }
  temp = head;
  cout << "------------------------" << endl;
  cout << "Elementy listy: " << endl;
  while (temp != NULL)
  {
    cout << temp->data << "->";
    temp = temp->next;
  }
  cout << "NULL" << endl;
  cout << "------------------------" << endl;
}

void linkedList::deleteList ()
{
  node *temp;
  while (head != NULL)
  {
    temp = head;
    head = head -> next;
    delete temp;
  }
  cout << "------------------------" << endl;
  cout << "Lista zostala usunieta." << endl;
  cout << "------------------------" << endl;
}

linkedList::~linkedList()
{
  deleteList();
}
