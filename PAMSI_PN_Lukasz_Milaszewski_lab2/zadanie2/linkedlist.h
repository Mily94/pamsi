#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#include "node.h"
#include <iostream>


class linkedList
{
  node * head;
public:
  linkedList();
  node* createNode(int);
  void insertBegin();
  void insertPos();
  void insertLast();
  void deletePos();
  void display();
  void deleteList();
  ~linkedList();
};


#endif // LINKEDLIST_H
