#include "menu.h"
#include "linkedList.h"

using namespace std;

void menu::displayMenu()
{
  linkedList link;
  int option;
  do
  {
    cout << "1.Dodaj element na poczatek" << endl;
    cout << "2.Dodaj element na koniec" << endl;
    cout << "3.Dodaj element na pozycje" << endl;
    cout << "4.Usun konkretna pozycje" << endl;
    cout << "5.Usun liste" << endl;
    cout << "6.Wyswietl liste" << endl;
    cout << "0.Koniec " << endl;
    cout << "Wybierz opcje: ";
    cin >> option;
    switch(option)
    {
      case 1:
        link.insertBegin();
        break;
      case 2:
        link.insertLast();       
        break;
      case 3:
        link.insertPos();      
        break;
      case 4:
        link.deletePos();       
        break;
      case 5:
        link.deleteList();
        break;
      case 6:
        link.display();
        break;
    }
  } while (option != 0);
}
