#include <iostream>
#include <fstream>
#include <ctime>
#include <string>

using namespace std;

void makeFiles(int,int);

int main()
{
  makeFiles(10000,1);
  makeFiles(50000,1);
  makeFiles(100000,1);
  makeFiles(500000,1);
  makeFiles(1000000,1);

  return 0;
}

void makeFiles(int length, int counter)
{
  srand( time( NULL ) );
  ofstream myFile;
  for (int i = 0; i < 100; i++)
  {
    string iter = to_string(counter);
    string temp = to_string(length);
    string fileName = "unsorted"+temp+"_" + iter + ".txt";
    counter ++;

    myFile.open(fileName.c_str());
    for ( int i = 0; i < length; i++)
      myFile << rand() << endl;
    myFile.close();
  }
}
