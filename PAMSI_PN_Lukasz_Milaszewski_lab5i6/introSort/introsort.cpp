#include "IntroSort.h"

template <typename T>
void IntroSort<T>::sort_intro(T *arr, int n, int M)
{
  int i;
  if(M <= 0)
  {
    heap_sort(arr,n);
    return;
  }
  i = partition(arr,0,n-1);
  if (i > 0)
    sort_intro(arr,i,M-1);
  if ((n-1-i) > 0)
    sort_intro(arr+i+1,n-1-i,M-1);
}

template <typename T>
int IntroSort<T>::partition(T *arr, int l, int p)
{
  int left = l ;
  int valueLeft = arr[left];
  swap(arr[left] , arr[p]);
  int now = l;

  for(int i = l; i < p; i++)
  {
    if(arr[i] < valueLeft)
    {
      swap(arr[i] , arr[now]);
      now++;
    }
  }
  swap(arr[now], arr[p]);
  return now;
}

template <typename T>
void IntroSort<T>::heap_sort(T *arr, int N)
{
  int i;
  for(i=N/2; i>0; --i)
    build_heap(arr-1,i,N);
  for(i=N-1; i>0; --i)
  {
    swap(arr[0],arr[i]);
    build_heap(arr-1,1,i);
  }
}

template <typename T>
void IntroSort<T>::build_heap(T *arr, int i, int N)
{
  int j;
  while (i<=N/2)
  {
    j=2*i;
    if (j+1<=N && arr[j+1]>arr[j])
      j=j+1;
    if (arr[i]<arr[j])
      swap(arr[i],arr[j]);
    else break;
    i=j;
  }
}

template <typename T>
void IntroSort<T>::isSorted(T *arr, int n)
{
  for (int i = 0; i < n-1; i++)
    if (arr[i]>arr[i+1])
      cout << "not sorted." << endl;
}

template <typename T>
void IntroSort<T>::swap(int &a, int &b)
{
  T t = a;
  a = b;
  b = t;
}

