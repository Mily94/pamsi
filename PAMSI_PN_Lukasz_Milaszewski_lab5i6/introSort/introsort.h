#ifndef INTROSORT_H
#define INTROSORT_H

#include <iostream>
#include <cmath>

template <class T>
class IntroSort
{
public:
  void sort_intro(T *, int, int);
  int partition(T *, int l, int p);
  void build_heap(T *, int i, int N);
  void heap_sort(T *, int N);
  void swap( int&, int&);
  void isSorted(T *, int);
};

#endif // INTROSORT_H
