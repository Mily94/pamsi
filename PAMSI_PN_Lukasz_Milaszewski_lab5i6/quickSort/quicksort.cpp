#include "quicksort.h"

template <typename T>
void quickSort<T>::display(T *arr, int length)
{
  for (int i = 1; i <= length; i++)
    cout << arr[i] << " ";
}

template <typename T>
void quickSort<T>::isSorted(T *arr, int n)
{
  for (int i = 0; i < n-1; i++)
    if (arr[i]>arr[i+1])
      cout << "not sorted." << endl;
}

template <typename T>
void quickSort<T>::swap(T &a, T &b)
{
  T t = a;
  a = b;
  b = t;
}

template <typename T>
void quickSort<T>::quick_sort(T *arr,int left,int right)
{
  if (left >= right)
    return;
  int middle = left;
  //int middle = left + (right - left) / 2;
  swap(arr[middle], arr[left]);
  int midpoint = partition(arr, left + 1, right, arr[left]);
  swap(arr[left], arr[midpoint]);
  quick_sort(arr, left, midpoint);
  quick_sort(arr, midpoint + 1, right);
}

template <typename T>
int quickSort<T>::partition(T *arr, int left, int right,int pivot)
{
  for (int i=left; i<right; ++i) {
    if (arr[i] <= pivot) {
      swap(arr[i], arr[left]);
      left ++;
    }
  }
  return left - 1;
}

