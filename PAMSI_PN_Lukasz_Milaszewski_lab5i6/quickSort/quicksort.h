#ifndef QUICKSORT_H
#define QUICKSORT_H

#include <iostream>

template <class T>
class quickSort
{
public:

  void quick_sort(T *, int, int);
  void swap(T&, T&);
  void display(T *, int);
  void isSorted(T *, int);
  int partition(T*, int, int, int);
};

#endif // QUICKSORT_H
