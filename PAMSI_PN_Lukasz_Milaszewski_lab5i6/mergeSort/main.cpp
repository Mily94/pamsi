#include "merge.cpp"
#include "handling.cpp"
#include <windows.h>
#include <iostream>

using namespace std;

void measureTime(int,string);

int main()
{

  string beginning = "opposite";
  measureTime(10000,beginning);
  measureTime(50000,beginning);
  measureTime(100000,beginning);
  measureTime(500000,beginning);
  measureTime(1000000,beginning);

  return 0;
}


void measureTime (int length, string beginning)
{
  LARGE_INTEGER frequency;        // ticks per second
  LARGE_INTEGER t1, t2;           // ticks
  double elapsedTime;
  double sum = 0;

  string fileName = beginning+to_string(length)+".txt";
  double *time = new double [100];

  Handling <int> k;
  mergeSort <int> p;


  k.makeContainer(length);
  k.readFile(length, fileName);

  for (int i = 0; i < 100; i++)
  {
    QueryPerformanceFrequency(&frequency);
    QueryPerformanceCounter(&t1);

    p.merge_sort(k.arr,1, length);

    QueryPerformanceCounter(&t2);

    p.isSorted(k.arr,length);
    elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
    time[i] = elapsedTime;
    sum += time[i];
  }
  k.writeFile(length);
  cout << length << " : "<< sum/100 << " ms." << endl;
  k.deleteContainer();
  delete [] time;
}
