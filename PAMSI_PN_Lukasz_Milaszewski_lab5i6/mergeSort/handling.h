#ifndef HANDLING_H
#define HANDLING_H

#include <fstream>
#include <string>

using namespace std;

template <class T>
class Handling {

public:
  T *arr;

  void makeContainer(int);
  void readFile(int, string);
  void writeFile(int);
  void deleteContainer();

};

#endif // HANDLING_H
