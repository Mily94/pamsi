#include "merge.h"

using namespace std;

template <typename T>
void mergeSort<T>::merge_sort(T *arr, int left, int right)
{
  if (left < right)
  {
    int middle = (left + right) / 2;
    merge_sort(arr, left, middle);
    merge_sort(arr, middle + 1, right);
    merge(arr, left, middle+1, right);
  }
}

template <typename T>
void mergeSort<T>::merge(T *arr, int leftStart, int rightStart, int rightEnd)
{
  int leftEnd = rightStart - 1;
  int length = rightEnd - leftStart + 1;
  T * temp = new T [length];
  int tempStart = 0;

  while (leftStart <= leftEnd && rightStart <= rightEnd)
  {
    if (arr[leftStart] <= arr[rightStart])
      temp[tempStart++] = arr[leftStart++];
    else
      temp[tempStart++] = arr[rightStart++];
  }

  while (leftStart <= leftEnd)
    temp[tempStart++] = arr[leftStart++];

  while (rightStart <= rightEnd)
    temp[tempStart++] = arr[rightStart++];

  for (int i = length - 1; i >= 0; i--)
    arr[rightEnd--] = temp[i];

  delete [] temp;
}

template <typename T>
void mergeSort<T>::display(T *arr,int length)
{
  for (int i = 1; i <= length; i++)
    cout << " i = " << i << "   " << arr[i] << endl;
}


template <typename T>
void mergeSort<T>::isSorted(T *arr,int n)
{
  for (int i = 1; i < n; i++)
    if (arr[i]>arr[i+1])
      cout << "not sorted." << endl;
}




