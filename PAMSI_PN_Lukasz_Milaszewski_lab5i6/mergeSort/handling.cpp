#include "handling.h"

using namespace std;

template <typename T>
void  Handling<T>::makeContainer(int length)
{
  arr = new T [length+1];
}

template <typename T>
void Handling<T>::readFile(int length,string fileName)
{
  ifstream myFile;
  myFile.open(fileName.c_str());

  for (int i = 1; i <= length; i++)
    myFile >> arr[i];

  myFile.close();
}

template <typename T>
void Handling<T>::writeFile(int length)
{
  ofstream myFile;
  myFile.open("test.txt");
  for ( int i =1; i <= length; i++)
    myFile << arr[i] << endl;
  myFile.close();
}

template <typename T>
void Handling<T>::deleteContainer()
{
  delete [] arr;
}


