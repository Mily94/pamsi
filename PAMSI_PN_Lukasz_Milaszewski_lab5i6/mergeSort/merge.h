#ifndef MERGE_H
#define MERGE_H

#include <iostream>

template <class T>
class mergeSort
{
public:

  void merge_sort(T *,int, int);
  void merge(T *, int, int, int);
  void display(T *,int);
  void isSorted(T *,int);

};

#endif // MERGE_H
