#include "wrapper.h"

#include <algorithm>
#include <iostream>

using namespace std;

Wrapper::Wrapper(){}

Wrapper::~Wrapper(){}

void Wrapper::init(int vertices, int edges, int start)
{
  _graph = new Graph();

  _graph -> set_first(start);

  _graph -> _matrix = matrix(vertices);

  for ( vector <int>& s : _graph -> _matrix)
    s = vector<int>(vertices);

  _graph -> _list = list(vertices);
}

void Wrapper::build_from_random(int v, int edges, int w)
{
  init(v, edges, w);

  vector <vector<int> > l(v);

  for (int i = 0; i < (int) l.size(); i++)
    l[i].push_back(i);

  int e = 0;
  while (e < edges)
  {
    int i = rand() % v;
    while ( (int) l[i].size() == v)
      i = rand() % v;

    if ((int) _graph -> _list[i].size() < v)
    {
      int to = i;

 // szukamy wierzcholka z ktorym nie ma polaczenia
      while (find(l[i].begin(), l[i].end(), to) != l[i].end())
        to = rand()%v;

      l[i].push_back(to);

      int cost = rand()%100-50;

      _graph -> add_edge(i, to, cost);
      e += 2;
    }
  }
}

void Wrapper::build_random(int vertices, int d, int w)
{
  start = w;
  edges = d*vertices*(vertices-1)/100;

  build_from_random(vertices,edges, w);
}

void Wrapper::build_from_file()
{
  cout << "Podaj nazwe pliku: ";
  string fileName;
  cin >> fileName;

  ifstream file(fileName);
  if(file.is_open())
  {
    parse_file(file);
  }
}

void Wrapper::save_to_file(string fileName)
{

  ofstream file(fileName);
  if(file.is_open())
  {
    save_file(file);
  }
}

void Wrapper::save_file(ofstream &file)
{
    file << "Wierzcholek poczatkowy: " << start << endl;
    for (int i = 0; i < (int)_path.dist.size(); i++)
    {
        file << i << " " << _path.dist[i] << " : ";
        int j = _path.prev[i];
        while (j != -1)
        {
            file << j << " ";
            j = _path.prev[j];
        }
        file << endl;
    }
}

void Wrapper::parse_file(ifstream &file)
{
  file >> vertices;
  file >> edges;
  file >> start;
  init(vertices,edges, start);

  int from,to,cost;
  while(!file.eof())
  {
    file >> from;
    file >> to;
    file >> cost;
   _graph -> add_edge(from, to, cost);
  }
}

void Wrapper::print_shortest_path()
{
    for(int i = 0; i<(int)_path.dist.size(); i++)
    {
        cout << "Wierzcholek: " << i << "\tOdleglosc: " << _path.dist[i]
            << "\tSciezka: ";

        int j = _path.prev[i];
        while(j != -1)
        {
            cout << j << " ";
            j = _path.prev[j];

        }
        cout << endl;
    }
}

void Wrapper::ford_l()
{
    _path = _ford->perform_l(_graph);
}

void Wrapper::ford_m()
{
    _path = _ford->perform_m(_graph);
}
