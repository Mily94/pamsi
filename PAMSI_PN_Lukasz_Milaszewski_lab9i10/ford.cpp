#include <vector>
#include <climits>
#include "ford.h"
class Graph;

using namespace std;

path Ford::perform_l(Graph *g)
{

    int size =g-> _list.size();

    bool test;

    vector<int> dist(size);
    vector<int> prev(size);

    path d;

    for(int i = 0; i < size; i++)
    {
        dist[i] = INT_MAX/2;
        prev[i] = -1;
    }

    dist[g->_first] = 0;


    for(int i = 1; i< size; i++)
    {

        test = true;
        for(int x = 0; x< size; x++)
        {
            vector<vecpair> &adj =g-> _list[x];
            for(int u = 0; u<(int)adj.size(); u++)
            {
                if(dist[ adj[u].first ] > dist[x] + adj[u].second)
                {
                    test = false;
                    dist[ adj[u].first ] = dist[x] + adj[u].second;
                    prev[ adj[u].first ] = x;
                }
            }
        }

        if(test)
        {
          d.dist = dist;
          d.prev = prev;
          return d;
        }
    }
// sprawdzenie ujemnych cykli
    for(int x = 0; x<size; x++)
    {
        vector<vecpair> &adj = g->_list[x];
        for(int u = 0; u<(int) adj.size(); u++)
        {
            if(dist[ adj[u].first ] > dist[x] + adj[u].second)
            {
                d.dist = vector<int>(0);
                d.prev = vector<int>(0);
                return d;
            }
        }
    }
    d.dist = dist;
    d.prev = prev;
    return d;
}

path Ford::perform_m(Graph *g)
{
    int size = g->_matrix.size();

    bool test;

    vector<int> dist(size);
    vector<int> prev(size);

    path d;

    for(int i = 0; i < size; i++)
    {
        dist[i] = INT_MAX/2;
        prev[i] = -1;
    }

    dist[g->_first] = 0;

    for(int i = 1; i < size; i++)
    {
        test = true;
        for(int x = 0; x< size; x++)
        {
            vector<int> &adj = g->_matrix[x];
            for(int u = 0; u<(int) adj.size(); u++)
            {

                    if(dist[u] > dist[x] + adj[u])
                    {
                        test = false;
                        dist[u] = dist[x] + adj[u];
                        prev[u] = x;
                    }

            }
        }
        if(test)
        {
           d.dist = dist;
           d.prev = prev;
           return d;
        }
    }

    for(int x = 0; x<size; x++)
    {
        vector<int> &adj = g->_matrix[x];
        for(int u = 0; u<(int)adj.size(); u++)
        {

                if(dist[u] > dist[x] + adj[u])
                {
                    d.dist = vector<int>(0);
                    d.prev = vector<int>(0);
                    return d;
                }

        }
    }
    d.dist = dist;
    d.prev = prev;
    return d;
}
