#ifndef WRAPPER_H
#define WRAPPER_H
#include "ford.h"
#include "graph.h"
#include <fstream>

class Wrapper
{
public:
  Wrapper();
  ~Wrapper();

  int edges;
  int vertices;
  int start;

  void init (int vertices, int edges, int start);
  void build_from_file();
  void build_random(int v, int density, int w);
  void build_from_random(int v, int edges, int w);
  void ford_l();
  void ford_m();
  void save_file(std::ofstream &file);
  void save_to_file(std::string fileName);
  void parse_file(std::ifstream &file);
  void print_shortest_path();

  Graph *_graph;
  path _path;
  Ford *_ford;
};

#endif // WRAPPER_H
