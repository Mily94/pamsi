#include "graph.h"
#include <iostream>
#include <queue>

using edge = std::pair<int, int>;
using wedge = std::pair<edge, int>;

using namespace std;

Graph::Graph(){}

Graph::~Graph(){}

void Graph::set_first(int first)
{
    _first = first;
}

void Graph::add_edge(int from, int to, int cost)
{
  _matrix[from][to] = cost;
  _list[from].push_back(make_pair(to,cost));
}
