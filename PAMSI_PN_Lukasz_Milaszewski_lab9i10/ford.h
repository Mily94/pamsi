#ifndef FORD_H
#define FORD_H
#include "graph.h"
#include <vector>

class path
{
public:
   std::vector<int> dist;
   std::vector<int> prev;

friend class Wrapper;
friend class Ford;
};

class Ford
{
    path perform_m(Graph *g);
    path perform_l(Graph *g);

    friend class Wrapper;
};

#endif // FORD_H
