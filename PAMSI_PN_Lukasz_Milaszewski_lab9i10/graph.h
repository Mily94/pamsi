#ifndef GRAPH_H
#define GRAPH_H

#include <vector>

using vecpair = std::pair<int,int>;
using edge = std::pair<int, int>;
using wedge = std::pair<edge, int>;
using matrix = std::vector< std::vector<int> >;
using list = std::vector< std:: vector <vecpair> >;

class Graph
{
public:
  Graph();
  ~Graph();
  void set_first(int first);
  void add_edge(int from, int to, int cost);

private:

  matrix _matrix;
  list _list;
  int _first;

  friend class Wrapper;
  friend class Ford;
};

#endif // GRAPH_H
