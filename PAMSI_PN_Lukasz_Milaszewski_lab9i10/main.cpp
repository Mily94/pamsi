#include "graph.h"
#include "wrapper.h"

#include <windows.h>
#include <iostream>

using namespace std;

void Timer(int vertices, int density);

int main()
{
  Timer(10, 25);
  Timer(50, 25);
  Timer(100, 25);
  Timer(500, 25);
  Timer(1000, 25);

  Timer(10, 50);
  Timer(50, 50);
  Timer(100, 50);
  Timer(500, 50);
  Timer(1000, 50);

  Timer(10, 75);
  Timer(50, 75);
  Timer(100, 75);
  Timer(500, 75);
  Timer(1000, 75);

  Timer(10, 100);
  Timer(50, 100);
  Timer(100, 100);
  Timer(500, 100);
  Timer(1000, 100);

  return 0;
}






















void Timer(int vertices, int density)
{

  int start = vertices / 2;
  LARGE_INTEGER frequency;        // ticks per second
  LARGE_INTEGER t1, t2;           // ticks


  QueryPerformanceFrequency(&frequency);

  double elapsedTimeL = 0;
  double elapsedTimeM = 0;
  double elapsedTimeList = 0;
  double elapsedTimeMatrix = 0;
  cout << "vertices: " << vertices << "  density: " << density << endl;
  cout << "++++++++++++++++++++++++++++++++++++++++++++" << endl;
  Wrapper p;
  for (int i = 0; i < 100; i++)
  {
    p.build_random(vertices,density,start);

    QueryPerformanceCounter(&t1);
    p.ford_l();
    QueryPerformanceCounter(&t2);
    elapsedTimeL = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
    cout << i << " : " << elapsedTimeL << endl;
    elapsedTimeList += elapsedTimeL;

    QueryPerformanceCounter(&t1);
    p.ford_m();
    QueryPerformanceCounter(&t2);
    elapsedTimeM = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
     cout << i << " : " << elapsedTimeM << endl;
    elapsedTimeMatrix += elapsedTimeM;
  }
  cout << "----------------------------------------------" << endl;
  cout << "List Ford results: " << elapsedTimeList / 100 << endl;
  cout << "Matrix Ford results: " << elapsedTimeMatrix / 100 << endl;
  cout << "----------------------------------------------" << endl;
  cout << endl << endl;

}

